using UnityEngine;

namespace ConveyorBelts
{
    public class PhysicsBased : MonoBehaviour
    {
        private const float MIN_TORQUE = 0.01f;
        private const float MAX_TORQUE = 1.0f;

        [SerializeField]
        private Rigidbody[] rollers;
        [SerializeField]
        [Range(MIN_TORQUE, MAX_TORQUE)]
        private float torque;
        public bool clockwiseRotation;
        
        private Vector3 TorqueVector
        {
            get => clockwiseRotation ? Vector3.down : Vector3.up;
        }

        public float Torque
        {
            get => torque;
            set
            {
                torque = Mathf.Clamp(value, MIN_TORQUE, MAX_TORQUE);
            } 
        }

        private void FixedUpdate()
        {
            for (int i = 0; i < rollers.Length; i++)
            {
                rollers[i]?.AddRelativeTorque(TorqueVector * torque, ForceMode.Force);
            }
        }
    }
}
