using System;
using UnityEngine;

namespace ConveyorBelts
{
    public class Trigger : MonoBehaviour
    {
        public event Action<Transform> OnStay;

        private void OnTriggerStay(Collider other) 
        {
            OnStay?.Invoke(other.transform);
        }
    }
}
