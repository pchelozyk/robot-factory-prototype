using UnityEngine;

namespace ConveyorBelts
{
    public class TriggerBased : MonoBehaviour
    {
        private const float MIN_SPEED = 0.0f;
        private const float MAX_SPEED = 100.0f;

        [SerializeField]
        private Trigger triggerSender;
        [SerializeField]
        [Range(MIN_SPEED, MAX_SPEED)]
        private float speed;
        public bool reverseDirection;

        public float Speed
        {
            get => speed;
            set
            {
                speed = Mathf.Clamp(value, MIN_SPEED, MAX_SPEED);
            }
        }

        private void Awake()
        {
            triggerSender.OnStay += HandleOnStay;
        }

        private void HandleOnStay(Transform other)
        {
            MoveToEdge(other);
        }

        private void MoveToEdge(Transform objectTransform)
        {
            Vector3 targetPosition = objectTransform.position;
            if (reverseDirection)
                targetPosition += transform.TransformDirection(Vector3.left);
            else
                targetPosition += transform.TransformDirection(Vector3.right);

            objectTransform.position = Vector3.MoveTowards(objectTransform.position, targetPosition,
                                                           speed * Time.deltaTime);
        }
    }
}
