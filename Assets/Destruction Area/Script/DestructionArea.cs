using UnityEngine;

namespace TriggerIndependent
{
    public class DestructionArea : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            Destroy(other.gameObject);
        }
    }
}